# vim: fileencoding=utf-8 tw=100 expandtab ts=4 sw=4 :

from __future__ import unicode_literals


from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, HTML
from django import forms
from django.utils.translation import ugettext_lazy as _, ugettext
from activkool.apps.carconnect.models import Vehicle, Spending
from activkool.apps.carconnect.widgets import ClearableImageInput


class CarAddForm(forms.ModelForm):
    class Meta:
        model = Vehicle
        fields = (
            'name',
            'picture',
            'koolbox_id',
        )
        widgets = {
            'picture': ClearableImageInput,
        }

    def __init__(self, owner, *args, **kwargs):
        super(CarAddForm, self).__init__(*args, **kwargs)
        self._owner = owner
        self.helper = FormHelper(self)

        self.setup_layout()

    def setup_layout(self):
        self.helper.layout = Layout(
            Fieldset(
                _('Votre voiture'),

                'name',
                'picture',
                'koolbox_id',
            ),

            HTML('<div class="row">'
                 '  <div class="col-sm-4 col-sm-offset-2">'
                 '      <button class="btn btn-primary" type="submit">{}</button>'
                 '  </div>'
                 '</div>'.format(ugettext('Ajouter'))),
        )

        self.helper.form_tag = False
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-sm-2'
        self.helper.field_class = 'col-sm-4'

    def save(self, commit=True):
        instance = super(CarAddForm, self).save(commit=False)

        instance.owner = self._owner

        if commit:
            instance.save()
            self.save_m2m()

        return instance


class SpendingForm(forms.ModelForm):
    class Meta:
        model = Spending
        fields = (
            'label',
            'bill',
            'amount',
        )

    def __init__(self, payer, vehicle, *args, **kwargs):
        super(SpendingForm, self).__init__(*args, **kwargs)
        self._payer = payer
        self._vehicle = vehicle

    def save(self, commit=True):
        instance = super(SpendingForm, self).save(commit=False)

        instance.payer = self._payer
        instance.vehicle = self._vehicle

        if commit:
            instance.save()
            self.save_m2m()

        return instance

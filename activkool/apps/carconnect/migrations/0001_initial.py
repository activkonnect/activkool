# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sorl.thumbnail.fields
import uuid_upload_path.storage


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Consumership',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
        ),
        migrations.CreateModel(
            name='Repayment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('amount', models.FloatField()),
                ('date', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='Spending',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('label', models.CharField(max_length=100, verbose_name='label')),
                ('bill', models.FileField(upload_to=uuid_upload_path.storage.upload_to, null=True, verbose_name='facture', blank=True)),
                ('amount', models.FloatField()),
                ('date', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='Vehicle',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('picture', sorl.thumbnail.fields.ImageField(help_text="Votre photo, de pr\xe9f\xe9rence dans un format carr\xe9 d'au moins 220px de c\xf4t\xe9", upload_to=uuid_upload_path.storage.upload_to, null=True, verbose_name='photo', blank=True)),
                ('longitude', models.FloatField()),
                ('latitude', models.FloatField()),
                ('koolbox_id', models.UUIDField()),
            ],
        ),
    ]

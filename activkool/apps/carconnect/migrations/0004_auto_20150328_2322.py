# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sorl.thumbnail.fields
from django.conf import settings
import uuid_upload_path.storage


class Migration(migrations.Migration):

    dependencies = [
        ('carconnect', '0003_auto_20150328_1810'),
    ]

    operations = [
        migrations.AddField(
            model_name='spending',
            name='vehicle',
            field=models.ForeignKey(default=None, to='carconnect.Vehicle'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='repayment',
            name='grantee',
            field=models.ForeignKey(related_name='received_payments', verbose_name='b\xe9n\xe9ficiaire', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='spending',
            name='label',
            field=models.CharField(max_length=100, verbose_name='libell\xe9'),
        ),
        migrations.AlterField(
            model_name='vehicle',
            name='picture',
            field=sorl.thumbnail.fields.ImageField(help_text="Votre photo, de pr\xe9f\xe9rence dans un format carr\xe9 d'au moins 220px de c\xf4t\xe9", upload_to=uuid_upload_path.storage.upload_to, null=True, verbose_name='photo', blank=True),
        ),
    ]

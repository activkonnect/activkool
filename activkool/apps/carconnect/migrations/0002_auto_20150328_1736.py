# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('carconnect', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='vehicle',
            name='consumers',
            field=models.ManyToManyField(related_name='lent_vehicles', through='carconnect.Consumership', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='vehicle',
            name='owner',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='spending',
            name='payer',
            field=models.ForeignKey(verbose_name='payeur', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='repayment',
            name='grantee',
            field=models.ForeignKey(related_name='received_payments', verbose_name='b\xe9n\xe9ficiaire', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='repayment',
            name='payer',
            field=models.ForeignKey(related_name='sent_payments', verbose_name='payeur', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='consumership',
            name='consumer',
            field=models.ForeignKey(related_name='c+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='consumership',
            name='vehicle',
            field=models.ForeignKey(related_name='v+', to='carconnect.Vehicle'),
        ),
    ]

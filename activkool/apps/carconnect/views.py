# vim: fileencoding=utf-8 tw=100 expandtab ts=4 sw=4 :

from __future__ import unicode_literals


from collections import defaultdict
import json
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.http.response import Http404, HttpResponse
from django.shortcuts import render, get_object_or_404, redirect
from django.utils.translation import ugettext as _
from activkool.apps.carconnect.forms import CarAddForm, SpendingForm
from activkool.apps.carconnect.models import Vehicle, Spending, Repayment, Consumership
from activkool.apps.register.models import User


@login_required
def car(request, pk):
    asked_car = get_object_or_404(Vehicle, pk=pk)

    return render(request, 'activkool/car.html', {
        'car': asked_car,
        'all_users': [u.email for u in (User.objects
                                        .exclude(pk=request.user.pk)
                                        .exclude(lent_vehicles=asked_car))],
    })


@login_required
def cars_list(request):
    cars = Vehicle.objects.filter(Q(owner=request.user) | Q(consumers=request.user)).distinct()

    return render(request, 'activkool/cars_list.html', {
        'cars': cars,
    })


@login_required
def add_car(request):
    if request.method == 'POST':
        form = CarAddForm(request.user, request.POST, request.FILES)

        if form.is_valid():
            form.save()
            messages.success(request, _('Votre voiture a été ajoutée'))
            return redirect('cars_list')
    else:
        form = CarAddForm(owner=request.user)

    return render(request, 'activkool/add_car.html', {
        'form': form,
    })


def make_sums(request, vehicle):
    sums = defaultdict(lambda: 0)
    for repayment in Repayment.objects \
            .filter(Q(payer=request.user) | Q(grantee=request.user)) \
            .filter(vehicle=vehicle):
        if repayment.payer != request.user:
            sums[repayment.payer] -= repayment.amount
        else:
            sums[repayment.grantee] += repayment.amount
    positive = {k: v for k, v in sums.items() if v > 0}
    negative = {k: -v for k, v in sums.items() if v < 0}
    sum_positive = sum(positive.values())
    sum_negative = sum(negative.values())
    return negative, positive, sum_negative, sum_positive


@login_required
def money(request, pk):
    vehicle = get_object_or_404(Vehicle, pk=pk)

    if request.method == 'GET' and request.GET.get('clear') is not None:
        cleared = get_object_or_404(User, pk=request.GET.get('clear'))
        negative, positive, sum_negative, sum_positive = make_sums(request, vehicle)
        Repayment.objects.create(
            payer=cleared,
            grantee=request.user,
            amount=positive[cleared],
            vehicle=vehicle
        )

    if request.method == 'POST':
        form = SpendingForm(request.user, vehicle, request.POST, request.FILES)

        if form.is_valid():
            form.save()
            messages.success(request, _('Votre facture a été enregistrée'))
        else:
            messages.success(request, _('Il y a eu une erreur lors de l\'ajout de votre facture'))

    negative, positive, sum_negative, sum_positive = make_sums(request, vehicle)

    return render(request, 'activkool/money.html', {
        'vehicle': vehicle,
        'bills': Spending.objects.filter(vehicle=vehicle).order_by('-date'),
        'positive': positive,
        'negative': negative,
        'sum_positive': sum_positive,
        'sum_negative': sum_negative,
    })


@login_required
def append_to_car(request, pk):
    vehicle = get_object_or_404(Vehicle, pk=pk)
    user = get_object_or_404(User, email=request.POST.get('email'))

    if vehicle.owner != request.user:
        raise Http404

    Consumership.objects.create(vehicle=vehicle, consumer=user)

    return HttpResponse(json.dumps({
        'status': 'ok',
    }), content_type="application/json")

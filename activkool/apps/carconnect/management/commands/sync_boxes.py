# vim: fileencoding=utf-8 tw=100 expandtab ts=4 sw=4 :

from django.core.management.base import BaseCommand
from activkool.apps.carconnect.kernel import get_box
from activkool.apps.carconnect.models import Vehicle


class Command(BaseCommand):
    def handle(self, *args, **options):
        for vehicle in Vehicle.objects.all():
            # noinspection PyBroadException
            try:
                state = get_box(vehicle.koolbox_id)
                vehicle.latitude = state['box']['latitude']
                vehicle.longitude = state['box']['longitude']
                vehicle.save()
            except:
                pass

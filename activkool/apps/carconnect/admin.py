# vim: fileencoding=utf-8 tw=100 expandtab ts=4 sw=4 :

from django.contrib import admin
from activkool.apps.register.models import User
from models import Vehicle, Consumership, Spending, Repayment

admin.site.register(User)
admin.site.register(Vehicle)
admin.site.register(Consumership)
admin.site.register(Spending)
admin.site.register(Repayment)

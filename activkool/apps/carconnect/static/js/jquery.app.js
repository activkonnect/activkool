/*vim: fileencoding=utf8 tw=100 expandtab ts=4 sw=4 */
/*jslint indent: 4, maxlen: 100 */
/*global jQuery,angular,L */

(function ($) {
    'use strict';
    // Variables

    var $map = $('#map'),
        lat = $map.attr('data-lat').replace(',', '.'),
        lng = $map.attr('data-lng').replace(',', '.'),
        map = L.map('map').setView([lat, lng], 11);

    L.tileLayer('https://{s}.tiles.mapbox.com/v4/vincentloy.lj8l6den/{z}/{x}/{y}.png' +
        '?access_token=pk.eyJ1IjoidmluY2VudGxveSIsImEiOiJ6MjdpUmM4In0.noTMgcA6cYJnQMArNLuAqQ', {
            maxZoom: 18,
            attribution: 'Map data &copy; <a href="http://openstreetmap.org">' +
                'OpenStreetMap</a> contributors, ' +
                '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
                'Imagery &copy; <a href="http://mapbox.com">Mapbox</a>'
        }).addTo(map);

    L.marker([lat, lng]).addTo(map)
        .bindPopup("Votre véhicule").openPopup();

}(jQuery));

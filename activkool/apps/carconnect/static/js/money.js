/*vim: fileencoding=utf8 tw=100 expandtab ts=4 sw=4 */
/*jslint indent: 4, maxlen: 100 */
/*global jQuery,angular */

(function (ng) {
    'use strict';

    var app = ng.module('carconnect');

    app.controller('MoneyCtrl', ['$scope', '$modal', function ($scope, $modal) {
        var addBill;

        addBill = function () {
            $modal.open({
                templateUrl: 'activkool/partials/money_modal.html',
                controller: 'MoneyModalCtrl'
            });
        };

        $scope.addBill = addBill;
    }]);

    app.controller('MoneyModalCtrl', ['$scope', function ($scope) {
        $scope.data = {};
    }]);
}(angular));

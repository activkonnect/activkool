/*jslint indent: 4 */
/*global angular, jQuery, document */

(function (ng, $) {
    'use strict';

    var app = ng.module('carconnect');

    app.controller('CarController', ['$scope', '$http', function (
        $scope,
        $http
    ) {
        var submitForm;

        submitForm = function () {
            if ($scope.allUsers.indexOf($scope.selected) >= 0) {
                $http({
                    method: 'POST',
                    url: 'append/',
                    data: $.param({
                        email: $scope.selected
                    }),
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                }).then(function () {
                    document.location.reload(true);
                });
            }
        };

        $scope.submitForm = submitForm;
    }]);
}(angular, jQuery));

/*jslint indent: 4 */
/*global angular */

(function (ng) {
    'use strict';

    var app = ng.module('carconnect', [
        'ui.bootstrap.modal',
        'ui.bootstrap',
        'ui.bootstrap.typeahead'
    ]);

    app.config(['$interpolateProvider', function ($interpolateProvider) {
        $interpolateProvider.startSymbol('((');
        $interpolateProvider.endSymbol('))');
    }]);

    app.run(['$http', function ($http) {
        $http.defaults.xsrfCookieName = 'csrftoken';
        $http.defaults.xsrfHeaderName = 'X-CSRFToken';
    }]);

    app.run(['$templateCache', function ($templateCache) {
        var tpl = $templateCache.get('template/typeahead/typeahead-match.html');
        tpl = tpl.replace(/\{\{/g, '(( ');
        tpl = tpl.replace(/\}\}/g, ' ))');
        $templateCache.put('template/typeahead/typeahead-match.html', tpl);
    }]);

    app.run(['$templateCache', function ($templateCache) {
        var tpl = $templateCache.get('template/typeahead/typeahead-popup.html');
        tpl = tpl.replace(/\{\{/g, '(( ');
        tpl = tpl.replace(/\}\}/g, ' ))');
        $templateCache.put('template/typeahead/typeahead-popup.html', tpl);
    }]);
}(angular));

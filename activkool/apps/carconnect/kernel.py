# vim: fileencoding=utf-8 tw=100 expandtab ts=4 sw=4 :

from __future__ import unicode_literals


import requests
from django.conf import settings


def get_box(box_id):
    r = requests.get(
        url=settings.KOOLICAR_ENDPOINT + '/box/{}/'.format(box_id),
        headers={
            'Authorization': 'Token token={}'.format(settings.KOOLICAR_TOKEN)
        }
    )

    return r.json()

# vim: fileencoding=utf-8 tw=100 expandtab ts=4 sw=4 :

from django.forms import ClearableFileInput
from django.template.loader import render_to_string


class ClearableImageInput(ClearableFileInput):
    def render(self, name, value, attrs=None):
        return render_to_string('activkool/partials/clearable_image_input.html', {
            'name': name,
            'checkbox_name': self.clear_checkbox_name(name),
            'url': value.url if value and hasattr(value, 'url') else None,
        })

# vim: fileencoding=utf-8 tw=100 expandtab ts=4 sw=4 :

from django.db import models
from django.utils.six import python_2_unicode_compatible
from django.utils.translation import gettext_lazy as _
from django.conf import settings
from sorl import thumbnail
from uuid_upload_path.storage import upload_to


@python_2_unicode_compatible
class Vehicle(models.Model):
    owner = models.ForeignKey(settings.AUTH_USER_MODEL)
    name = models.CharField(max_length=100)
    picture = thumbnail.ImageField(
        upload_to=upload_to,
        null=True,
        blank=True,
        verbose_name=_('photo'),
        help_text=_('Votre photo, de préférence dans un format carré d\'au moins 220px de côté')
    )
    longitude = models.FloatField(null=True, blank=True)
    latitude = models.FloatField(null=True, blank=True)
    koolbox_id = models.UUIDField()
    consumers = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        through='Consumership',
        related_name='lent_vehicles',
    )

    def __str__(self):
        return u'{} "{}"'.format(self.owner, self.name)


class Consumership(models.Model):
    vehicle = models.ForeignKey('Vehicle', related_name='v+')
    consumer = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='c+')


class Spending(models.Model):
    payer = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_('payeur'))
    label = models.CharField(max_length=100, verbose_name=_('libellé'))
    bill = models.FileField(upload_to=upload_to, null=True, blank=True, verbose_name=_('facture'))
    vehicle = models.ForeignKey('Vehicle')
    amount = models.FloatField()
    date = models.DateTimeField(auto_now_add=True)

    def save(self, *args, **kwargs):
        super(Spending, self).save(*args, **kwargs)

        cnt = self.vehicle.consumers.all().count() + 1
        owed = float(self.amount) / float(cnt)

        for target in list(self.vehicle.consumers.all()) + [self.vehicle.owner]:
            if target != self.payer:
                Repayment.objects.create(
                    payer=self.payer,
                    grantee=target,
                    amount=owed,
                    vehicle=self.vehicle,
                )


class Repayment(models.Model):
    payer = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_('payeur'),
        related_name='sent_payments',
    )
    grantee = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_('bénéficiaire'),
        related_name='received_payments',
    )
    amount = models.FloatField()
    date = models.DateTimeField(auto_now_add=True)
    vehicle = models.ForeignKey('Vehicle')

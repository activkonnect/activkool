# vim: fileencoding=utf-8 tw=100 expandtab ts=4 sw=4 :
#
# activkool
# (c) 2014 ActivKonnect

from django.shortcuts import render, redirect


def landing(request):
    if request.user.is_authenticated():
        return redirect('cars_list')

    return render(request, 'activkool/landing.html')

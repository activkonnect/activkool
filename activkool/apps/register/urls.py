# vim: fileencoding=utf-8 tw=100 expandtab ts=4 sw=4 :
#
# activkool
# (c) 2014 ActivKonnect

from django.conf.urls import patterns
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = patterns(
    'activkool.apps.register.views',
) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

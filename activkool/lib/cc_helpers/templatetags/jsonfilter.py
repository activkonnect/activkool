# vim: fileencoding=utf-8 tw=100 expandtab ts=4 sw=4 :

import json as json_mod
from json import encoder
from django import template
from django.core.serializers.json import DjangoJSONEncoder
from django.utils import six
from django.utils.encoding import smart_text

register = template.Library()


def is_alphanumeric(c):
    return 0x30 <= c <= 0x39 \
        or 0x41 <= c <= 0x5A \
        or 0x61 <= c <= 0x7A


def is_js_safe_ascii(c):
    return (0x20 <= c <= 0x7e) and c != 0x3c and c != 0x3e


def base_encode_basestring(s, is_safe, escape_map):
    s = smart_text(s)

    def char_to_js(c):
        n = ord(c)

        if n in escape_map:
            return escape_map[n]
        elif is_safe(n):
            return c
        elif n < 0x10000:
            return '\\u{0:04x}'.format(n)
        else:
            n -= 0x10000
            s1 = 0xd800 | ((n >> 10) & 0x3ff)
            s2 = 0xdc00 | (n & 0x3ff)
            return '\\u{0:04x}\\u{1:04x}'.format(s1, s2)

    # noinspection PyCallingNonCallable
    output = six.StringIO()
    output.write('"')
    for char in s:
        output.write(char_to_js(char))
    output.write('"')

    value = output.getvalue()
    output.close()

    return value


def script_safe_encode_basestring(s):
    return base_encode_basestring(s, is_alphanumeric, {})


def script_lax_encode_basestring(s):
    return base_encode_basestring(s, is_js_safe_ascii, {
        0x0d: '\\r',
        0x0a: '\\n',
        0x22: '\\"',
    })


@register.filter
def json(value, lax=False):
    old_basestring_encoder = encoder.encode_basestring_ascii
    encoder.encode_basestring_ascii = \
        script_safe_encode_basestring if not lax else script_lax_encode_basestring
    encoded = json_mod.dumps(value, ensure_ascii=True, cls=DjangoJSONEncoder)
    encoder.encode_basestring_ascii = old_basestring_encoder

    return encoded

# vim: fileencoding=utf-8 tw=100 expandtab ts=4 sw=4 :
#
# activkool
# (c) 2014 ActivKonnect

from .common import *

DEBUG = True
TEMPLATE_DEBUG = True

MEDIA_URL = '/media/'
MEDIA_ROOT = DJANGO_ROOT.child('media')

EMAIL_BACKEND = 'django.core.mail.backends.filebased.EmailBackend'
EMAIL_FILE_PATH = '/tmp/gmp-emails'

# vim: fileencoding=utf-8 tw=100 expandtab ts=4 sw=4 :
#
# activkool
# (c) 2014 ActivKonnect

PIPELINE_COMPILERS = (
    'pylesswrap.pipeline_compiler.PyLessWrapCompiler',
)

PIPELINE_CSS = {
    'bootstrap': {
        'source_filenames': (
            'activkool/bootstrap/bootstrap.less',
            'less/car.less',
            'less/cars_list.less',
            'less/money.less',
            'less/layout.less',
            'less/landing.less',
        ),
        'output_filename': 'css/bootstrap.css',
    },
}

PIPELINE_JS = {
    'libs': {
        'source_filenames': (
            'js/common.js',
            'js/car.js',
            'js/money.js',
            'js/jquery.app.js',
        ),
    }
}

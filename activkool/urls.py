# vim: fileencoding=utf-8 tw=100 expandtab ts=4 sw=4 :
#
# activkool
# (c) 2014 ActivKonnect

from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin

urlpatterns = patterns(
    '',

    url('^$', 'activkool.apps.register.views.landing', name='landing'),
    url('^car/(?P<pk>\d+)/$', 'activkool.apps.carconnect.views.car', name='car'),
    url('^car/(?P<pk>\d+)/money/$', 'activkool.apps.carconnect.views.money', name='money'),
    url('^car/(?P<pk>\d+)/append/$', 'activkool.apps.carconnect.views.append_to_car', name='append'),
    url('^home/$', 'activkool.apps.carconnect.views.cars_list', name='cars_list'),
    url('^add_car/$', 'activkool.apps.carconnect.views.add_car', name='add_car'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^accounts/', include('activkool.apps.register.urls')),
) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) \
  + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
